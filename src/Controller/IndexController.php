<?php

namespace App\Controller;

use App\Form\ContactType;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        // DBAL de doctrine
        /** @var Connection $cnx */
        $cnx = $this->getDoctrine()->getConnection();

        $query = <<<SQL
SELECT *
FROM article
WHERE title LIKE :title
SQL;

        $stmt = $cnx->prepare($query);
        $stmt->bindValue(':title', '%e%');

        $stmt->execute();

        dump($stmt->fetchAll());

        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);

        // pré-remplissage des champs si l'utilisateur est connecté
        if (!is_null($this->getUser())) {
            $form->get('name')->setData($this->getUser());
            $form->get('email')->setData($this->getUser()->getEmail());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                $contactEmail = $this->getParameter('contact_email');
                $mail = $mailer->createMessage();
                $mailBody = $this->renderView(
                    'index/contact_body.html.twig',
                    [
                        'data' => $data
                    ]
                );

                $mail
                    ->setSubject('Nouveau message sur votre blog')
                    ->setFrom($contactEmail)
                    ->setTo($contactEmail)
                    ->setBody($mailBody, 'text/html')
                    ->setReplyTo($data['email'])
                ;

                $mailer->send($mail);

                $this->addFlash(
                    'success',
                    'Votre message est envoyé'
                );
            } else {
                $this->addFlash(
                    'error',
                    'Le formulaire contient des erreurs'
                );
            }
        }

        return $this->render(
            'index/contact.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
