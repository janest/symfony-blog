<?php


namespace App\Controller\Admin;


use App\Entity\Article;
use App\Form\ArticleType;
use App\Form\SearchArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller\Admin
 *
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(
        Request $request,
        ArticleRepository $articleRepository
    )
    {
        $searchForm = $this->createForm(SearchArticleType::class);

        $searchForm->handleRequest($request);

        dump($searchForm->getData());

        $articles = $articleRepository->search((array)$searchForm->getData());

        return $this->render(
            'admin/article/index.html.twig',
            [
                'articles' => $articles,
                'search_form' => $searchForm->createView()
            ]
        );
    }

    /**
     * Faire le rendu du formulaire et son traitement
     * Mettre un lien ajouter dans la page de liste
     *
     * Validation : tous les champs obligatoires
     *
     * En création :
     *  - setter l'auteur avec l'utilisateur connecté
     *      ($this->getUser() depuis un contrôleur)
     *  - setter la date de publication à maintenant
     *
     * si validation ok : enregistrement en bdd et redirection vers la liste
     * avec message flash
     *
     * titre : TextType
     * contenu : TextareaType
     * catégorie : EntityType
     *
     * Adapter la route et le contenu de la méthode pour la modification
     * et ajouter le bouton modifier dans la liste
     *
     * @Route("/edition/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id)
    {
        $originalImage = null;

        if (is_null($id)) {
            $article = new Article();
            $article->setAuthor($this->getUser());
        } else {
            $article = $em->find(Article::class, $id);

            if (is_null($article)) {
                throw new NotFoundHttpException();
            }

            $originalImage = $article->getImage();

            if (!is_null($originalImage)) {
                // on sette l'image avec un objet File pour le champ de formulaire
                $article->setImage(
                    new File($this->getParameter('upload_dir') . $originalImage)
                );
            }
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var UploadedFile $image */
                $image = $article->getImage();

                // s'il y a une image uploadée
                if (!is_null($image)) {
                    // nom sous lequel on va enregistrer l'image
                    $filename = uniqid() . '.' . $image->guessExtension();

                    // déplace l'image uploadée
                    $image->move(
                        // dans quel répertoire (cf config/services.yaml)
                        $this->getParameter('upload_dir'),
                        $filename
                    );

                    $article->setImage($filename);

                    // suppression de l'ancienne image s'il y en a une
                    if (!is_null($originalImage)) {
                        unlink($this->getParameter('upload_dir') . $originalImage);
                    }
                } else {
                    // pour une image non changée en modification
                    $article->setImage($originalImage);
                }

                $em->persist($article);
                $em->flush();

                $this->addFlash('success', "L'article est enregistré");

                return $this->redirectToRoute('app_admin_article_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'admin/article/edit.html.twig',
            [
                'form' => $form->createView(),
                'original_image' => $originalImage
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     * @param Article $article
     */
    public function delete(
        EntityManagerInterface $em,
        Article $article
    ) {
        $titre = $article->getTitle();

        $em->remove($article);
        $em->flush();

        $this->addFlash('success', "L'article \"$titre\" est supprimé");

        return $this->redirectToRoute('app_admin_article_index');
    }
}
